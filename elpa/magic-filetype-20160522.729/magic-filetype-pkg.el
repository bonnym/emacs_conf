;;; -*- no-byte-compile: t -*-
(define-package "magic-filetype" "20160522.729" "Enhance filetype major mode" '((emacs "24") (s "1.9.0")) :url "https://github.com/zonuexe/magic-filetype.el" :keywords '("vim" "ft" "file" "magic-mode"))
