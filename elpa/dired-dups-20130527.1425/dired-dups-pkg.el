;;; -*- no-byte-compile: t -*-
(define-package "dired-dups" "20130527.1425" "Find duplicate files and display them in a dired buffer" 'nil :url "https://github.com/vapniks/dired-dups" :keywords '("unix"))
