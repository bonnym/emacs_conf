;;; -*- no-byte-compile: t -*-
(define-package "pycarddavel" "20150831.516" "Integrate pycarddav" '((helm "1.7.0") (emacs "24.0")) :keywords '("helm" "pyccarddav" "carddav" "message" "mu4e" "contacts"))
