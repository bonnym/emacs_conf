;;; pycarddavel-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "pycarddavel" "pycarddavel.el" (22515 32765
;;;;;;  0 0))
;;; Generated autoloads from pycarddavel.el

(autoload 'pycarddavel-search-with-helm "pycarddavel" "\
Start helm to select your contacts from a list.
If REFRESH is not-nil, make sure to ask pycarrdav to refresh the contacts
list.  Otherwise, use the contacts previously fetched from pycarddav.

\(fn REFRESH)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; pycarddavel-autoloads.el ends here
