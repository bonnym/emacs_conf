;;; -*- no-byte-compile: t -*-
(define-package "helm-recoll" "20160731.221" "helm interface for the recoll desktop search tool." '((helm "1.9.9")) :url "https://github.com/emacs-helm/helm-recoll" :keywords '("convenience"))
