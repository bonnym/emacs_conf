;;; -*- no-byte-compile: t -*-
(define-package "dired-quick-sort" "20160524.338" "Persistent quick sorting of dired buffers in various ways." '((hydra "0.13.0")) :url "https://gitlab.com/xuhdev/dired-quick-sort#dired-quick-sort" :keywords '("convenience" "files"))
