;;; -*- no-byte-compile: t -*-
(define-package "helm-purpose" "20160218.1009" "Helm Interface for Purpose" '((emacs "24") (helm "1.9.2") (window-purpose "1.4")) :url "https://github.com/bmag/helm-purpose")
