;;; -*- no-byte-compile: t -*-
(define-package "helm-ros" "20160812.1052" "Interfaces ROS with helm" '((helm "1.9.9") (xterm-color "1.0") (cl-lib "0.5")) :url "https://www.github.com/davidlandry93/helm-ros" :keywords '("helm" "ros"))
