;;; -*- no-byte-compile: t -*-
(define-package "magic-latex-buffer" "20160212.603" "Magically enhance LaTeX-mode font-locking for semi-WYSIWYG editing" '((cl-lib "0.5") (emacs "24.3")) :url "http://hins11.yu-yake.com/")
