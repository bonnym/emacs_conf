;;; rails-log-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "rails-log-mode" "rails-log-mode.el" (22515
;;;;;;  32722 0 0))
;;; Generated autoloads from rails-log-mode.el

(autoload 'rails-log-show-development "rails-log-mode" "\


\(fn)" t nil)

(autoload 'rails-log-show-test "rails-log-mode" "\


\(fn)" t nil)

(autoload 'rails-log-show-production "rails-log-mode" "\


\(fn)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; rails-log-mode-autoloads.el ends here
