;;; -*- no-byte-compile: t -*-
(define-package "py-test" "20151116.2222" "A test runner for Python code." '((dash "2.9.0") (f "0.17") (emacs "24.4")) :url "https://github.com/Bogdanp/py-test.el" :keywords '("python" "testing" "py.test"))
