;;; helm-words-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "helm-words" "helm-words.el" (22515 34053 0
;;;;;;  0))
;;; Generated autoloads from helm-words.el

(autoload 'helm-words-dict-pl-search "helm-words" "\
Show results from the dict.pl dictionary for the given WORD.

\(fn WORD)" t nil)

(autoload 'helm-words "helm-words" "\
Run helm-words.

\(fn)" t nil)

(autoload 'helm-words-at-point "helm-words" "\
Run helm-words with thing at point.

\(fn)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; helm-words-autoloads.el ends here
