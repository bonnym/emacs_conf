;;; -*- no-byte-compile: t -*-
(define-package "dired-explorer" "20160809.200" "minor-mode provides Explorer like select file at dired." '((cl-lib "0.5")) :keywords '("dired" "explorer"))
