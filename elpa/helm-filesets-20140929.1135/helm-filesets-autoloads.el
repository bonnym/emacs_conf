;;; helm-filesets-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "helm-filesets" "helm-filesets.el" (22515 34151
;;;;;;  0 0))
;;; Generated autoloads from helm-filesets.el

(autoload 'helm-make-source-filesets "helm-filesets" "\


\(fn FSNAME)" nil nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; helm-filesets-autoloads.el ends here
