;;; -*- no-byte-compile: t -*-
(define-package "helm-jstack" "20150602.2122" "Helm interface to Jps & Jstack for Java/JVM processes" '((emacs "24") (helm "1.7.0") (cl-lib "0.5")) :keywords '("java" "jps" "jstack" "jvm" "emacs" "elisp" "helm"))
