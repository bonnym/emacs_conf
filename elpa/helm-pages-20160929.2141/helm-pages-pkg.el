;;; -*- no-byte-compile: t -*-
(define-package "helm-pages" "20160929.2141" "Pages in current buffer as Helm datasource" '((helm "1.6.5") (emacs "24") (cl-lib "0.5")) :keywords '("convenience" "helm" "outlines"))
