;;; direnv-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "direnv" "direnv.el" (22515 35784 0 0))
;;; Generated autoloads from direnv.el

(autoload 'direnv-load-environment "direnv" "\
Load the direnv environment for FILE-NAME.
If FILE-NAME not provided, default to the current buffer.

\(fn &optional FILE-NAME)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; direnv-autoloads.el ends here
