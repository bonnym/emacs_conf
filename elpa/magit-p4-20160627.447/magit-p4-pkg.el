;;; -*- no-byte-compile: t -*-
(define-package "magit-p4" "20160627.447" "git-p4 plug-in for Magit" '((magit "2.1") (magit-popup "2.1") (p4 "12.0") (cl-lib "0.5")) :url "https://github.com/qoocku/magit-p4" :keywords '("vc" "tools"))
