;;; helm-chronos-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "helm-chronos" "helm-chronos.el" (22515 34188
;;;;;;  0 0))
;;; Generated autoloads from helm-chronos.el

(autoload 'helm-chronos-add-timer "helm-chronos" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("helm-chronos-pkg.el") (22515 34188 0
;;;;;;  0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; helm-chronos-autoloads.el ends here
