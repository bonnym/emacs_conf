(define-package "helm-chronos" "20150528.1336" "helm interface for chronos timers"
  '((chronos "1.2")
    (helm "1.7.1"))
  :url "http://github.com/dxknight/helm-chronos" :keywords
  '("calendar"))
;; Local Variables:
;; no-byte-compile: t
;; End:
