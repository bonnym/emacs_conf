;;; -*- no-byte-compile: t -*-
(define-package "helm-hayoo" "20151013.2351" "Source and configured helm for searching hayoo" '((helm "1.6.0") (json "1.2") (haskell-mode "13.7")) :keywords '("helm"))
