;;; helm-hayoo-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "helm-hayoo" "helm-hayoo.el" (22515 34125 0
;;;;;;  0))
;;; Generated autoloads from helm-hayoo.el

(autoload 'helm-hayoo "helm-hayoo" "\
Preconfigured helm to search hayoo.

\(fn)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; helm-hayoo-autoloads.el ends here
