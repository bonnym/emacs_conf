;;; helm-sheet-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "helm-sheet" "helm-sheet.el" (22515 34067 0
;;;;;;  0))
;;; Generated autoloads from helm-sheet.el

(autoload 'helm-sheet "helm-sheet" "\
Helm to list sheets and to create a new sheet.

\(fn)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; helm-sheet-autoloads.el ends here
