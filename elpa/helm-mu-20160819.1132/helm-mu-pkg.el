;;; -*- no-byte-compile: t -*-
(define-package "helm-mu" "20160819.1132" "Helm sources for searching emails and contacts" '((helm "1.5.5")) :url "https://github.com/emacs-helm/helm-mu")
