;;; -*- no-byte-compile: t -*-
(define-package "helm-hoogle" "20160913.1022" "Use helm to navigate query results from Hoogle" '((helm "1.6.2") (emacs "24.4")) :url "https://github.com/jwiegley/haskell-config" :keywords '("haskell" "programming" "hoogle"))
