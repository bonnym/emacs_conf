;;; -*- no-byte-compile: t -*-
(define-package "helm-org-rifle" "20160926.1239" "Rifle through your Org files" '((emacs "24.4") (dash "2.12") (f "0.18.1") (helm "1.9.4") (s "1.10.0")) :url "http://github.com/alphapapa/helm-org-rifle" :keywords '("hypermedia" "outlines"))
