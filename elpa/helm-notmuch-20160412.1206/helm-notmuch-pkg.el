;;; -*- no-byte-compile: t -*-
(define-package "helm-notmuch" "20160412.1206" "Search emails with Notmuch and Helm" '((helm "1.9.3") (notmuch "0.21")) :url "https://github.com/xuchunyang/helm-notmuch" :keywords '("mail"))
