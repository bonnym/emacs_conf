;;; -*- no-byte-compile: t -*-
(define-package "helm-hunks" "20160720.24" "A helm interface for git hunks" '((emacs "24.4") (helm "1.9.8")) :keywords '("helm" "git" "hunks" "vc"))
