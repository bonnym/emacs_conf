;;; helm-hunks-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "helm-hunks" "helm-hunks.el" (22515 34117 0
;;;;;;  0))
;;; Generated autoloads from helm-hunks.el

(autoload 'helm-hunks "helm-hunks" "\
Helm-hunks entry point.

\(fn)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; helm-hunks-autoloads.el ends here
