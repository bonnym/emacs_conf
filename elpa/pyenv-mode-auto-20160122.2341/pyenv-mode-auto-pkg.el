;;; -*- no-byte-compile: t -*-
(define-package "pyenv-mode-auto" "20160122.2341" "Automatically activates pyenv version if .python-version file exists." '((pyenv-mode "0.1.0") (s "1.11.0") (f "0.17.0")) :url "https://github.com/ssbb/pyenv-mode-auto" :keywords '("python" "pyenv"))
