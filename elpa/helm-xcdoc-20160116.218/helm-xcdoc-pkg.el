;;; -*- no-byte-compile: t -*-
(define-package "helm-xcdoc" "20160116.218" "Search Xcode Document by docsetutil and eww with helm interface" '((helm "1.5") (emacs "24.4")) :url "https://github.com/fujimisakari/emacs-helm-xcdoc")
