;;; -*- no-byte-compile: t -*-
(define-package "helm-cider" "20160912.1935" "Helm interface to CIDER" '((emacs "24.4") (cider "0.12") (helm-core "2.0") (seq "1.0")) :url "https://github.com/clojure-emacs/helm-cider" :keywords '("tools" "convenience"))
