;;; ember-yasnippets-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "ember-yasnippets" "ember-yasnippets.el" (22515
;;;;;;  38611 0 0))
;;; Generated autoloads from ember-yasnippets.el

(autoload 'ember-yasnippets-initialize "ember-yasnippets" "\


\(fn)" nil nil)

(eval-after-load "yasnippet" '(ember-yasnippets-initialize))

;;;***

;;;### (autoloads nil nil ("ember-yasnippets-pkg.el") (22515 38611
;;;;;;  0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; ember-yasnippets-autoloads.el ends here
