(define-package "sage-shell-mode" "20161002.122" "A front-end for Sage Math"
  '((cl-lib "0.5")
    (deferred "0.4.0")
    (emacs "24.1"))
  :url "https://github.com/sagemath/sage-shell-mode" :keywords
  '("sage" "math"))
;; Local Variables:
;; no-byte-compile: t
;; End:
