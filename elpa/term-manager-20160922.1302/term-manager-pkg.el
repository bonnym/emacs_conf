(define-package "term-manager" "20160922.1302" "Contextual terminal management"
  '((dash "2.12.0")
    (emacs "24.4"))
  :url "https://www.github.com/IvanMalison/term-manager" :keywords
  '("term" "manager"))
;; Local Variables:
;; no-byte-compile: t
;; End:
