;;; -*- no-byte-compile: t -*-
(define-package "bm" "20160924.1310" "Visible bookmarks in buffer." 'nil :url "https://github.com/joodland/bm" :keywords '("bookmark" "highlight" "faces" "persistent"))
