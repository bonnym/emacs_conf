;;; -*- no-byte-compile: t -*-
(define-package "term-projectile" "20161003.1428" "projectile terminal management" '((term-manager "0.1.0") (projectile "0.13.0")) :url "https://www.github.com/IvanMalison/term-manager" :keywords '("term" "manager" "projectile"))
