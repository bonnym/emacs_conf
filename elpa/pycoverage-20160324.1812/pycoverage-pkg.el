(define-package "pycoverage" "20160324.1812" "Support for coverage stats on Python 2.X and 3"
  '((emacs "24.3"))
  :url "https://github.com/mattharrison/pycoverage.el" :keywords
  '("project" "convenience"))
;; Local Variables:
;; no-byte-compile: t
;; End:
