;;; -*- no-byte-compile: t -*-
(define-package "helm-hatena-bookmark" "20160528.614" "Hatena::Bookmark with helm interface" '((helm "1.9.5")) :url "https://github.com/masutaka/emacs-helm-hatena-bookmark")
