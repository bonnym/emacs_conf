;;; helm-pass-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "helm-pass" "helm-pass.el" (22515 34092 0 0))
;;; Generated autoloads from helm-pass.el

(autoload 'helm-pass "helm-pass" "\
Helm interface for pass

\(fn)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; helm-pass-autoloads.el ends here
