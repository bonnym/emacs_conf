;;; -*- no-byte-compile: t -*-
(define-package "helm-pass" "20160825.1410" "helm interface of pass, the standard Unix password manager" '((helm "0") (password-store "0")) :url "https://github.com/jabranham/helm-pass")
