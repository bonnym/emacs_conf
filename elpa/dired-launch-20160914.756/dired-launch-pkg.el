;;; -*- no-byte-compile: t -*-
(define-package "dired-launch" "20160914.756" "Use dired as a launcher" 'nil :url "https://github.com/thomp/dired-launch" :keywords '("dired" "launch"))
