;;; -*- no-byte-compile: t -*-
(define-package "helm-spaces" "20161001.709" "helm sources for spaces" '((helm-core "2.2") (spaces "0.1.0")) :url "https://github.com/yasuyk/helm-spaces" :keywords '("helm" "frames" "convenience"))
