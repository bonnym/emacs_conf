;;; -*- no-byte-compile: t -*-
(define-package "helm-ghq" "20160203.727" "ghq with helm interface" '((helm "1.8.0")) :url "https://github.com/masutaka/emacs-helm-ghq")
