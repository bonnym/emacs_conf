;;; qiita-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "qiita" "qiita.el" (22515 32725 0 0))
;;; Generated autoloads from qiita.el

(autoload 'qiita:post "qiita" "\


\(fn &optional PRIVATE\\=\\?)" t nil)

(autoload 'qiita:stock "qiita" "\


\(fn)" t nil)

(autoload 'qiita:unstock "qiita" "\


\(fn)" t nil)

(autoload 'qiita:items "qiita" "\


\(fn &optional MY)" t nil)

(autoload 'qiita:user-items "qiita" "\


\(fn USER)" t nil)

(autoload 'qiita:tags "qiita" "\


\(fn)" t nil)

(autoload 'qiita:my-stocks "qiita" "\


\(fn)" t nil)

(autoload 'qiita:user-stocks "qiita" "\


\(fn USER)" t nil)

(autoload 'qiita:search "qiita" "\


\(fn &optional STOCKED)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; qiita-autoloads.el ends here
