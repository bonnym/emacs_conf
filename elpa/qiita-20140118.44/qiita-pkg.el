;;; -*- no-byte-compile: t -*-
(define-package "qiita" "20140118.44" "Qiita API Library for emacs" '((helm "1.5.9") (markdown-mode "2.0")) :url "https://github.com/gongo/qiita-el" :keywords '("qiita"))
