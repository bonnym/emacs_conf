;;; helm-rhythmbox-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "helm-rhythmbox" "helm-rhythmbox.el" (22515
;;;;;;  34074 0 0))
;;; Generated autoloads from helm-rhythmbox.el

(autoload 'helm-rhythmbox "helm-rhythmbox" "\
Choose a song from the Rhythmbox library to play or enqueue.

\(fn)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; helm-rhythmbox-autoloads.el ends here
