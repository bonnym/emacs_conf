;;; -*- no-byte-compile: t -*-
(define-package "helm-rhythmbox" "20160524.458" "control Rhythmbox's play queue via Helm" '((helm "1.5.0") (cl-lib "0.5")) :url "https://github.com/mrBliss/helm-rhythmbox")
