(use-package ivy-hydra :ensure t)


(use-package ace-window
  :ensure t
  :config
  (global-set-key (kbd "C-c w") 'ace-window))

(use-package ace-jump-mode
  :ensure t
  :config
  (define-key global-map (kbd "C-c SPC") 'ace-jump-mode))








(provide 'ivy-settings)
