

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(anzu-deactivate-region t)
 '(anzu-mode-lighter "")
 '(anzu-replace-threshold 50)
 '(anzu-replace-to-string-separator " => ")
 '(anzu-search-threshold 1000)
 '(custom-safe-themes
   (quote
    ("a800120841da457aa2f86b98fb9fd8df8ba682cebde033d7dbf8077c1b7d677a" "fc368b83d1944a438a69b83a7199405ecd419c2f14054a407e6d6e1a30bfddda" "946e871c780b159c4bb9f580537e5d2f7dba1411143194447604ecbaf01bd90c" "251348dcb797a6ea63bbfe3be4951728e085ac08eee83def071e4d2e3211acc3" "01e067188b0b53325fc0a1c6e06643d7e52bc16b6653de2926a480861ad5aa78" "73a13a70fd111a6cd47f3d4be2260b1e4b717dbf635a9caee6442c949fad41cd" "cf284fac2a56d242ace50b6d2c438fcc6b4090137f1631e32bedf19495124600" "962dacd99e5a99801ca7257f25be7be0cebc333ad07be97efd6ff59755e6148f" default)))
 '(initial-frame-alist (quote ((fullscreen . maximized))))
 '(org-agenda-files (quote ("~/Desktop/Prelude/todo.org")))
 '(package-selected-packages
   (quote
    (evil-smartparens smartparens-config textmate darktooth-theme json-rpc json-mode flymake-json smartparens emmet emmets swiper-helm conda counsel-dash counsel-osx-app counsel-projectile coverage ivy-pages evil org-bullets google-weather google-wheather google-contacts rainbow browse-kill-ring drag-stuff fix-word expand-region neotree datetime-separator true spaceline restclient git-gutter-fringe clojure-snippets highlight-parentheses paredit-everywhere paredit wakatime-mode htmlize all-the-icons command-log-mode arjen-grey-theme dynamic-fonts ace-jump-mode which-key kivy-mode ivy-hydra counsel use-package ac-emmet web-beautify airline-themes jedi-direx term-projectile whole-line-or-region fullframe anzu goto-chg discover-my-major smartscan rspec-mode feature-mode rinari rvm ibuffer-vc yari ruby-refactor ruby-hash-syntax ruby-additional ruby-block ruby-tools goto-gem yard-mode fiplr jump sass-mode scss-mode projectile-rails bundler sourcemap git-timemachine js2-mode jedi ob-ipython nxhtml better-defaults yafolding ido-vertical-mode visual-regexp-steroids buffer-move visual-regexp exec-path-from-shell swiper elpy auto-complete-nxml php-auto-yasnippets ember-yasnippets elm-yasnippets elixir-yasnippets auto-yasnippet ein-mumamo web-mode edbi-django pony-mode tern-django edbi yaml-tomato yaml-mode undo-tree rbenv rake rainbow-mode rainbow-identifiers rainbow-delimiters rainbow-blocks railscasts-theme rails-new rails-log-mode qml-mode qiita pyvenv python-x python-test python-mode python-info python-environment python-docstring python-django python-cell pytest pylint pyimpsort pyimport pyfmt pyenv-mode-auto pydoc-info pydoc pycoverage pycarddavel py-yapf py-test py-smart-operator py-isort py-import-check py-gnitset py-autopep8 nyan-mode nginx-mode monokai-theme magithub magit-topgit magit-svn magit-stgit magit-rockstar magit-p4 magit-gitflow magit-gh-pulls magit-gerrit magit-find-file magit-filenotify magit-annex magic-latex-buffer magic-filetype javaimp ivy heroku-theme heroku hemisu-theme help-mode+ help-fns+ help+ helm-zhihu-daily helm-xcdoc helm-words helm-wordnet helm-w3m helm-w32-launcher helm-unicode helm-themes helm-systemd helm-swoop helm-spotify helm-spaces helm-smex helm-sheet helm-sage helm-safari helm-rubygems-org helm-rubygems-local helm-ros helm-robe helm-rhythmbox helm-recoll helm-rb helm-rails helm-qiita helm-pydoc helm-purpose helm-pt helm-prosjekt helm-projectile helm-project-persist helm-proc helm-phpunit helm-perldoc helm-pass helm-pages helm-package helm-orgcard helm-org-rifle helm-open-github helm-notmuch helm-nixos-options helm-mu helm-mt helm-mode-manager helm-migemo helm-make helm-ls-svn helm-ls-hg helm-ls-git helm-lobsters helm-jstack helm-j-cheatsheet helm-itunes helm-ispell helm-img-tiqav helm-idris helm-hunks helm-hoogle helm-helm-commands helm-hayoo helm-hatena-bookmark helm-gtags helm-growthforecast helm-grepint helm-google helm-go-package helm-gitlab helm-gitignore helm-github-stars helm-git-grep helm-git-files helm-git helm-ghq helm-ghc helm-fuzzy-find helm-fuzzier helm-flyspell helm-flymake helm-flycheck helm-flx helm-firefox helm-filesets helm-emms helm-emmet helm-dirset helm-dired-recent-dirs helm-dired-history helm-dictionary helm-describe-modes helm-descbinds helm-dash helm-ctest helm-css-scss helm-cscope helm-company helm-commandlinefu helm-codesearch helm-cmd-t helm-clojuredocs helm-circe helm-cider-history helm-cider helm-chronos helm-chrome helm-c-yasnippet helm-c-moccur helm-bundle-show helm-bm helm-bind-key helm-bibtexkey helm-bibtex helm-backup helm-aws helm-anything helm-ag helm-ad helm-ack google-translate google-this google-maps golden-ratio ein djvu django-theme django-snippets django-mode django-manage discover direx-grep direnv diredful dired-toggle-sudo dired-toggle dired-subtree dired-sort-menu+ dired-sort dired-single dired-ranger dired-rainbow dired-quick-sort dired-open dired-narrow dired-launch dired-k dired-imenu dired-filter dired-filetype-face dired-fdclone dired-explorer dired-efap dired-dups dired-details+ dired-avfs dired-atool dired+ darkroom darkokai-theme cython-mode cyberpunk-theme csv-mode company-math coffee-mode chess beacon auto-install auto-complete auto-compile auctex ace-window))))

(require 'package) ;; You might already have this line
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(when (< emacs-major-version 24)
  ;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize) ;; You might already have this line

(when (not package-archive-contents)
  (package-refresh-contents))




(setq inhibit-splash-screen t
      initial-scratch-message nil
      initial-major-mode 'org-mode)


(setenv "PYTHONPATH" "the_path_which_python_command_returned")

(display-time-mode 1)

(global-linum-mode t)

(global-visual-line-mode t)

(delete-selection-mode t)

(setq auto-save-default nil)

(setq make-backup-file nil)

(fset 'yes-or-no-p 'y-or-n-p)

(tooltip-mode 1)

(electric-indent-mode t)

;; Make dired less verbose
(require 'dired-details)
(setq-default dired-details-hidden-string "--- ")
(dired-details-install)



;; for shufflling lines in coding
(defun move-line-down ()
  (interactive)
  (let ((col (current-column)))
    (save-excursion
      (forward-line)
      (transpose-lines 1))
    (forward-line)
    (move-to-column col)))

(defun move-line-up ()
  (interactive)
  (let ((col (current-column)))
    (save-excursion
      (forward-line)
      (transpose-lines -1))
    (move-to-column col)))

(global-set-key (kbd "<C-S-down>") 'move-line-down)
(global-set-key (kbd "<C-S-up>") 'move-line-up)




;;With these shortcuts you can open a new line above or below the current one, even if the cursor is midsentence.
(defun open-line-below ()
  (interactive)
  (end-of-line)
  (newline)
  (indent-for-tab-command))

(defun open-line-above ()
  (interactive)
  (beginning-of-line)
  (newline)
  (forward-line -1)
  (indent-for-tab-command))

(global-set-key (kbd "<C-return>") 'open-line-below)
(global-set-key (kbd "<C-S-return>") 'open-line-above)




;; full screen magit-status

(defadvice magit-status (around magit-fullscreen activate)
  (window-configuration-to-register :magit-fullscreen)
  ad-do-it
  (delete-other-windows))

(defun magit-quit-session ()
  "Restores the previous window configuration and kills the magit buffer"
  (interactive)
  (kill-buffer)
  (jump-to-register :magit-fullscreen))

(define-key magit-status-mode-map (kbd "q") 'magit-quit-session)



(setq warning-minimum-level :emergency)

(recentf-mode 1)
(setq recentf-max-saved-items 500)
(setq recentf-max-menu-items 60)
(global-set-key [(meta f12)] 'recentf-open-files)

(show-paren-mode 1)

(setq my-required-packages
      (list 'magit
            'swiper ;; visual regex search
            'exec-path-from-shell
            'visual-regexp
            'buffer-move ;; used for rotating buffers
            'visual-regexp-steroids
            'ido-vertical-mode
            'yafolding
            's
            'flycheck
            'elpy
            'py-autopep8
            'better-defaults
            'ein
            'pydoc-info
            'popup
            'jedi
            'nyan-mode
            'helm
            'js2-mode
            'yaml-mode
            'coffee-mode
            'git-timemachine ;; Walk through git revisions of a file
            'sourcemap
            'bundler
            'projectile
            'projectile-rails
            'scss-mode
            'sass-mode
            'f
            'jump
            'discover
            'fiplr
            'yard-mode ;; fontification in ruby comments
            'goto-gem ;; Open dired in a gem directory
            'ruby-tools
            'ruby-block
            'ruby-additional
            'ruby-hash-syntax
            'ruby-refactor
            'dired-details
            'yasnippet
            'yari
            'ibuffer-vc
            'rvm
            'rinari
            'web-mode
            'feature-mode
            'auto-compile
            'yaml-mode
            'rspec-mode
            'undo-tree
            'inf-ruby
            'smartscan ;; Quickly jumps between other symbols found at point in Emacs
            'discover-my-major
            'goto-chg
            'anzu
            'fullframe
            'markdown-mode
            'auctex
            'whole-line-or-region
            ))

(require 'undo-tree)
(global-undo-tree-mode 1)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


(mapc #'(lambda (package)
    (unless (package-installed-p package)
      (package-install package)))
      my-required-packages)





;;I use these two literally all the time. The first one removes trailing whitespace and replaces all tabs with spaces before save.
(defun cleanup-buffer-safe ()
  "Perform a bunch of safe operations on the whitespace content of a buffer.
Does not indent buffer, because it is used for a before-save-hook, and that
might be bad."
  (interactive)
  (untabify (point-min) (point-max))
  (delete-trailing-whitespace)
  (set-buffer-file-coding-system 'utf-8))

;; Various superfluous white-space. Just say no.
(add-hook 'before-save-hook 'cleanup-buffer-safe)

(defun cleanup-buffer ()
  "Perform a bunch of operations on the whitespace content of a buffer.
Including indent-buffer, which should not be called automatically on save."
  (interactive)
  (cleanup-buffer-safe)
  (indent-region (point-min) (point-max)))

(global-set-key (kbd "C-c n") 'cleanup-buffer)


;; making paredit work with delete-selection-mode
(put 'paredit-forward-delete 'delete-selection 'supersede)
(put 'paredit-backward-delete 'delete-selection 'supersede)
(put 'paredit-open-round 'delete-selection t)
(put 'paredit-open-square 'delete-selection t)
(put 'paredit-doublequote 'delete-selection t)
(put 'paredit-newline 'delete-selection t)

;; Move more quickly
(global-set-key (kbd "C-S-n")
                (lambda ()
                  (interactive)
                  (ignore-errors (next-line 5))))

(global-set-key (kbd "C-S-p")
                (lambda ()
                  (interactive)
                  (ignore-errors (previous-line 5))))

(global-set-key (kbd "C-S-f")
                (lambda ()
                  (interactive)
                  (ignore-errors (forward-char 5))))

(global-set-key (kbd "C-S-b")
                (lambda ()
                  (interactive)
                  (ignore-errors (backward-char 5))))


;;I want to collapse this paragraph-tag to one line
(global-set-key (kbd "M-j")
            (lambda ()
                  (interactive)
                  (join-line -1)))

;; after deleting a tag, indent properly
(defadvice sgml-delete-tag (after reindent activate)
  (indent-region (point-min) (point-max)))


;;airline themes
(require 'airline-themes)
(load-theme 'airline-light)

(load-theme 'monokai)

;;Bookmarks
(use-package bm
  :ensure t
  :bind (("C-c =" . bm-toggle)
         ("C-c [" . bm-previous)
         ("C-c ]" . bm-next)))




;;magit
(use-package magit
  :ensure t
  :bind (("C-x g" . magit-status)))

;;magit / Display the buffer state in the fringe.
(use-package git-gutter-fringe
  :ensure t
  :diminish git-gutter-mode
  :config
  (setq git-gutter-fr:side 'right-fringe)
  (set-face-foreground 'git-gutter-fr:modified "#63747c")
  (set-face-foreground 'git-gutter-fr:added    "#63747c")
  (set-face-foreground 'git-gutter-fr:deleted  "#63747c")
  (global-git-gutter-mode +1))






(use-package command-log-mode
  :ensure t)

(defun live-coding ()
  (interactive)
  (set-face-attribute 'default nil :font "Hack-16")
  (add-hook 'prog-mode-hook 'command-log-mode))

(use-package htmlize
  :ensure t)


;;look and feel
(global-prettify-symbols-mode 1)


;;snippets
(use-package yasnippet
  :ensure t
  :diminish yas
  :config
  (yas/global-mode 1)
  (add-to-list 'yas-snippet-dirs "~/.emacs.d/snippets"))

(use-package django-snippets
  :ensure t)


;;Auto completion
(use-package company
  :ensure t
  :bind (("C-c /". company-complete))
  :config
  (global-company-mode)
  )


;;REST support
(use-package restclient
  :ensure t)

;;neotree
(use-package neotree
  :ensure t
  :config
  )

(use-package expand-region
  :ensure t
  :config
  (global-set-key (kbd "C-=") 'er/expand-region))


(use-package fix-word
  :ensure t
  :config
  (global-set-key (kbd "M-u") #'fix-word-upcase)
  (global-set-key (kbd "M-l") #'fix-word-downcase)
  (global-set-key (kbd "M-c") #'fix-word-capitalize))

(use-package drag-stuff
  :ensure t
  :config
  (drag-stuff-global-mode 1))


(use-package browse-kill-ring
  :ensure t
  )


(use-package google-maps
  :ensure t
  )

(use-package google-contacts
  :ensure t)

(use-package which-key
  :ensure t
  :config
  (which-key-mode))

(use-package smartparens
  :ensure t
  :config
  ;; Always start smartparens mode in js-mode.
  (add-hook 'js-mode-hook #'smartparens-mode))

; emacs cursor color

(defvar blink-cursor-colors (list  "#006400" "#A9A9A9" "#F0FFFF" "#FF6347")
  "On each blink the cursor will cycle to the next color in this list.")

(setq blink-cursor-count 0)
(defun blink-cursor-timer-function ()
  "Zarza wrote this cyberpunk variant of timer `blink-cursor-timer'.
Warning: overwrites original version in `frame.el'.

This one changes the cursor color on each blink. Define colors in `blink-cursor-colors'."
  (when (not (internal-show-cursor-p))
    (when (>= blink-cursor-count (length blink-cursor-colors))
      (setq blink-cursor-count 0))
    (set-cursor-color (nth blink-cursor-count blink-cursor-colors))
    (setq blink-cursor-count (+ 1 blink-cursor-count))
    )
  (internal-show-cursor nil (not (internal-show-cursor-p)))
  )

;;custon.el ends here
